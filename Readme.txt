﻿=== Google SEO Pressor for Rich snippets === 
Contributors: smackcoders 
Donate link: https://www.paypal.me/smackcoders
Tags: admin, google, author, blog, Facebook, twitter, page, post, seo, social, microdata, rich snippets.
Requires at least: 4.4
Tested up to: 4.6.1
Stable tag: 1.3
Version: 1.3
Author: smackcoders
Author URI: http://profiles.wordpress.org/smackcoders/
License: GPLv2 or later


Enrich your WordPress website content with Google Structured Data and make it more SEO friendly.


== Description == 


Google SEO Author Snippet Plugin automatically structures your website content with [schema.org](http://schema.org/). The standardization helps you to come up in Google search with easy addition of SEO friendly snippets. You can uniquely define for each Post, Page, Custom Post and Product. You can also add social shares to your website authors to catch followers.


To know more about structuring your data, you can refer [structured data documentation](https://developers.google.com/search/docs/guides/intro-structured-data?visit_id=1-636106703325382676-1657924414&rd=1)


= Supports all snippets as recommended by Google =


* Rich snippets - Events
* Rich snippets - Music
* Rich snippets - Organizations
* Rich snippets - People
* Rich snippets - Products
* Rich snippets - Recipes
* Rich snippets - Reviews
* Rich snippets - Software applications
* Rich snippets - Videos: Facebook Share and RDFa


= Advantages =
* Easily rank your website content in Google. 
* Social shares for author to build social relationships. 
* Turn visitor into Customer.
* Reduces overall bounce rate.
* Encourages users to register before commenting on Posts


= Helpful links =


Visit [Smackcoders](https://www.smackcoders.com) to explore more WordPress products. You can write to us your comments, feedback at [support@smackcoders.com](mailto:support@smackcoders.com)


Get to know about other WordPress addons and latest product update news at [www.smackcoders.com/blogs.html](https://www.smackcoders.com/blog/category/wordpress-products)


== Installation ==


I. For simple general way to install


* Go to Plugins → Add New → Upload
* Browse and Upload the google-seo-author-snippets.zip file
* Activate the plugin.
* Configure in Google SEO Pressor Rich Snippets → Settings 


II. For familiar FTP users


* Extract google-seo-author-snippets.zip in /wp-content/plugins/
* Go to Plugins → Installed Plugins → Active 
* Configure in Google SEO Pressor Rich Snippets → Settings        


III. Straight from WordPress Admin


* Go to plugins → Add New
* Search for Google SEO Pressor Rich Snippets 
* Click Install Now → Activate
* Configure in Google SEO Pressor Rich Snippets → Settings




== Frequently Asked Questions == 


How to add social profiles for author ?<br />
To add social profiles for author, <br />
* Go to Google SEO Pressor Rich Snippets → Settings.<br />
* Enable Google plus, Twitter, Facebook, LinkedIn, GEO location. <br />
* You can enable it individual as per your requirements.<br />
* You can find the social profile in the edit view of the User.


How to configure Google snippets?<br />
You can uniquely configure the snippets for each Post, Page, Custom Post and Products. <br />
* Choose the post type and select the snippet type from the dropdown and save.<br />
* Now you can view the snippets in the Add or Edit view of the post types.


== Screenshots == 


1. Admin settings for Google SEO Pressor Rich Snippets Plugin . 
2. Google SEO Pressor Rich Snippets Post edit view 
3. Microdata Social profile information and User Geo-Information for user profile settings. 
4. Preview form placed in widget area to the front-end.
5. Remove option for applied Rich Snippets. 


== Changelog ==


= 1.3 =
* Added: WordPress 4.6.1 compatibility.
* Fixed: SQL injection vulnerabilities. 
* Modified: Generic Class & Function names as unique.
* Improved: Google Rich snippets for Videos, Software Apps & Events.


= 1.2.7 =
* Fixed: Vulnerability security issue.


= 1.2.6 =
* Fixed: Plugin Activation issues
* Fixed: Google Rich snippets changed for Events and Reviews.  


= 1.2.5 =
* Added: Remove option for applied rich snippets
* Fixed: Displays only the selected rich snippet in each post types
* Fixed: Minor bugs are fixed


= 1.2.4 =
* Fixed: Webmaster Testing Tool Warnings
* Empty schema content for music group
* Schema invalid data operating system version in software            
* Schema invalid data published date in software and item reviewed changed in review
* Added: Role in Person
* Added: Album Duration time in music
* Added: Price in product


= 1.2.3 =
* Added: filled in information only display in schema
* Fixed: some warnings fixed


= 1.2.2 =
* Added: WordPress 4.1 compatibility.
* Added: Google rich snippets for Reviews 
* Fixed: Clients side issues and warnings
* Fixed: UI works fixed 


= 1.2.1 =
* Modified: Menu Order changes added to avoid the blank page issues.
* Added: Dynamic Debug mode enable/disable feature in settings module.


= 1.2.0 =                
* Completely revamped to meet current google structured data requirement 
* Supports more than 11 snippets
* Added: Google rich snippets for People.
* Added: Google rich snippets for Product.
* Added: Google rich snippets for Events.
* Added: Google rich snippets for Organisation.
* Added: Google rich snippets for Recipes.
* Added: Google rich snippets for Review.
* Added: Google rich snippets for Music.
* Added: Google rich snippets for Software applications.
* Added: Google rich snippets for Videos: Facebook Share and RDFa.
* Added: Setting the google for each available post types.
* Added: Auto/Manual support only for (Google rich snippets-product).
* Automated snippets with easy mapping
* Can override post wide
* Option to apply post types and category wise
* WordPress 4.0 compatibility checked.


= 1.1.0 =        
* Important bug fix, IE Compatible added. Now compatible with WordPress-3.5.


= 1.0.1 =        
* Image Conflicts Bug Fixed, Added Extra Social Icon sets.


= 1.0.0 =
* Initial release version. Tested and found works well without any issues. 




== Upgrade Notice == 


= 1.3 = 
Upgrade now for WordPress 4.6.1 compatibility and improved Google Rich snippets of Videos, Software Apps & Events.


= 1.2.7 =           
Upgrade for security fix.


= 1.2.6 =           
Upgrade for more bug fixes


= 1.2.5 =           
Upgrade for bug fixes and added remove option for snippets


= 1.2.4 =           
Upgrade for bug free experience


= 1.2.3 =           
Upgrade for more bug fixes


= 1.2.2 =           
Upgrade for reviews and bug fixes


= 1.2.1 =           
Upgrade for dynamic debug mode enable/disable feature.


= 1.2.0 =        
Upgrade for newly added new snippets and automation


= 1.1.0 =        
Bug fix - Important bug fix, IE Compatible added. Now compatible with WordPress-3.5.


= 1.0.1 =        
Bug fix - Image conflicts, Added Extra Social Icon sets.


= 1.0.0 =        
Initial release of plugin
