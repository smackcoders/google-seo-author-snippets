<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function smack_google_seo_schema_software($text) {
	global $post;
	$prefix = 'google_snippets';
	// Get the product values for schema
	$google_seo_software_name = get_post_meta( $post->ID, $prefix.'software_name', true );
	$google_seo_software_operationg_systems = get_post_meta( $post->ID, $prefix.'software_operationg_systems', true );
	$google_seo_software_category = get_post_meta( $post->ID, $prefix.'software_category', true );
	$google_seo_software_version = get_post_meta( $post->ID, $prefix.'software_version', true );
	$google_seo_software_url = get_post_meta( $post->ID, $prefix.'software_url', true );
	$google_seo_software_image = get_post_meta( $post->ID, $prefix.'software_image', true );
	$google_seo_software_author = get_post_meta( $post->ID, $prefix.'software_author', true );
	$google_seo_software_aggregate_rating = get_post_meta( $post->ID, $prefix.'software_aggregate_rating', true );
	$google_seo_software_aggregate_rating_count = get_post_meta( $post->ID, $prefix.'software_aggregate_rating_count', true );
	$google_seo_software_price = get_post_meta( $post->ID, $prefix.'software_price', true );
	$google_seo_software_price_currency = get_post_meta( $post->ID, $prefix.'software_price_currency', true );
	$smack_google_seo_schema_software ="";
	$smack_google_seo_schema_software .= '<div style="display: none;" itemscope itemtype="http://schema.org/SoftwareApplication">';
	if(isset($google_seo_software_name))
		$smack_google_seo_schema_software .= '<span itemprop="name">'.$google_seo_software_name.'</span> -';
	if(isset($google_seo_software_image))
		$smack_google_seo_schema_software .= '<img itemprop="image" src="'.$google_seo_software_image.'"/>';

	$smack_google_seo_schema_software .= 'REQUIRES <span itemprop="operatingSystem">'.$google_seo_software_operationg_systems.'</span> <span itemprop="SoftwareVersion">'.$google_seo_software_version.'</span> and up';

	// Aggregate Rating
	$smack_google_seo_schema_software .= '<link itemprop="applicationCategory" href="http://schema.org/' .$google_seo_software_category . '"/>';
	$smack_google_seo_schema_software .= '<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">';
	$smack_google_seo_schema_software .= '<span itemprop="ratingValue">'.$google_seo_software_aggregate_rating.'</span> ( <span itemprop="ratingCount">'.$google_seo_software_aggregate_rating_count.'</span> ratings )';
	$smack_google_seo_schema_software .= "</div>";

	// Offer - Price & Price Currency
	$smack_google_seo_schema_software .= '<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">';
	if(isset($google_seo_software_price))
		$smack_google_seo_schema_software .= ' Price: <span itemprop="price">'.$google_seo_software_price.'</span>';
	$smack_google_seo_schema_software .= '<meta itemprop="priceCurrency" content="'. $google_seo_software_price_currency .'" />';
	$smack_google_seo_schema_software .= '</div>';

	$smack_google_seo_schema_software .= '</div>';
	return $text.$smack_google_seo_schema_software;

}

function smack_google_seo_schema_add_software() {
	global $post;
	$prefix = 'google_snippets';
	$google_seo_software_name = get_post_meta( $post->ID, $prefix.'software_name', true );
	if( $google_seo_software_name != '' && !is_home() ) {
		add_filter( "the_content", "smack_google_seo_schema_software" );
	}
}
add_action( 'wp', 'smack_google_seo_schema_add_software' );