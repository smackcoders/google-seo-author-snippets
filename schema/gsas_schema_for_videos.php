<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function smack_google_seo_schema_videos($text) {
        global $post;
        $prefix = 'google_snippets';
        // Get the product values for schema
        $google_seo_video_title          = get_post_meta( $post->ID, $prefix.'video_name', true );
        $google_seo_video_image_src      = get_post_meta( $post->ID, $prefix.'video_image_src', true );
        $google_seo_video_video_src      = get_post_meta( $post->ID, $prefix.'video_video_src', true );
        $google_seo_upload_date          = get_post_meta( $post->ID, $prefix.'upload_date', true );
        $google_seo_expire_date          = get_post_meta( $post->ID, $prefix.'expire_date', true );
        $google_seo_video_type           = get_post_meta( $post->ID, $prefix.'video_type', true );
        $google_seo_video_description    = get_post_meta( $post->ID, $prefix.'video_description', true );
        $google_seo_video_duration       = get_post_meta( $post->ID, $prefix.'video_duration', true );
        $google_seo_embed_url            = get_post_meta( $post->ID, $prefix.'embed_url', true );
        $google_seo_video_interaction_count = get_post_meta( $post->ID, $prefix.'interactionCount', true );

        $smack_google_seo_schema_videos = '';
        $smack_google_seo_schema_videos .= '<div itemscope itemtype="http://schema.org/VideoObject">';
        if(isset($google_seo_video_title))
                $smack_google_seo_schema_videos .= '<span itemprop="name">' . $google_seo_video_title . '</span>';
        if(isset($google_seo_video_description))
                $smack_google_seo_schema_videos .= '<span itemprop="description">' . $google_seo_video_description . '</span>';
        if(isset($google_seo_video_image_src))
                $smack_google_seo_schema_videos .= '<img itemprop="thumbnailUrl" src="' . $google_seo_video_image_src . '" alt="' . $google_seo_video_title . '" />';
        if(isset($google_seo_upload_date))
                $smack_google_seo_schema_videos .= '<meta itemprop="uploadDate" content="' . $google_seo_upload_date . '" />';
        if(isset($google_seo_video_duration))
                $smack_google_seo_schema_videos .=' <meta itemprop="duration" content="'.$google_seo_video_duration.'" />';
        if(isset($google_seo_video_video_src))
                $smack_google_seo_schema_videos .= '<link itemprop="contentURL" href="' . $google_seo_video_video_src . '"/>';
        if(isset($google_seo_embed_url))
                $smack_google_seo_schema_videos .= '<link itemprop="embedURL" href="' . $google_seo_embed_url . '" />';
        if(isset($google_seo_video_interaction_count))
                $smack_google_seo_schema_videos .= '<meta itemprop="interactionCount" content="' . $google_seo_video_interaction_count . '" />';
        $smack_google_seo_schema_videos .= '</div>';
        return $text.$smack_google_seo_schema_videos;
}

function smack_google_seo_schema_add_video() {
        global $post;
        $prefix = 'google_snippets';
        $google_seo_video_name = get_post_meta( $post->ID, $prefix.'video_name', true );
        if( $google_seo_video_name != '' && !is_home() ) {
                add_filter( "the_content", "smack_google_seo_schema_videos" );
        }
}
add_action( 'wp', 'smack_google_seo_schema_add_video' );