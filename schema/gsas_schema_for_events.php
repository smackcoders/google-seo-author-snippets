<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function smack_google_seo_schema_events($text) {
        global $post;
        $prefix = 'google_snippets';
        // Get the product values for schema
        $google_seo_events_summary = get_post_meta( $post->ID, $prefix.'events_summary', true );
        $google_seo_events_url = get_post_meta( $post->ID, $prefix.'events_url', true );
        $google_seo_events_photo = get_post_meta( $post->ID, $prefix.'events_photo', true );
        $google_seo_events_location = get_post_meta( $post->ID, $prefix.'events_location', true );
        $google_seo_events_description = get_post_meta( $post->ID, $prefix.'events_description', true );
        $google_seo_events_startdate = get_post_meta( $post->ID, $prefix.'events_startdate', true );
        $google_seo_events_enddate = get_post_meta( $post->ID, $prefix.'events_enddate', true );
        $google_seo_events_street_address = get_post_meta( $post->ID, $prefix.'events_street_address', true );
        $google_seo_events_locality = get_post_meta( $post->ID, $prefix.'events_locality', true );
        $google_seo_events_region = get_post_meta( $post->ID, $prefix.'events_region', true );
        $google_seo_events_country = get_post_meta( $post->ID, $prefix.'events_country', true );
        $google_seo_events_longitude = get_post_meta( $post->ID, $prefix.'events_longitude', true );
        $google_seo_events_latitude = get_post_meta( $post->ID, $prefix.'events_latitude', true );
        $google_seo_events_offer_aggregate = get_post_meta( $post->ID, $prefix.'events_offer_aggregate', true );
        $google_seo_low_price = get_post_meta( $post->ID, $prefix.'low_price', true );
        $google_seo_high_price = get_post_meta( $post->ID, $prefix.'high_price', true );
        $google_seo_offer_name = get_post_meta( $post->ID, $prefix.'offer_name', true );
        $google_seo_offer_category = get_post_meta( $post->ID, $prefix.'offer_category', true );
        $google_seo_offer_url = get_post_meta( $post->ID, $prefix.'offer_url', true );
        $google_seo_events_performer = get_post_meta( $post->ID, $prefix.'events_performer', true );
        $google_seo_events_type = get_post_meta( $post->ID, $prefix.'events_type', true );
        $google_seo_events_website = get_post_meta( $post->ID, $prefix.'events_website', true );
        $google_seo_events_ticket_price = get_post_meta( $post->ID, $prefix.'events_ticket_price', true );
        $google_seo_events_ticket_quantity = get_post_meta( $post->ID, $prefix.'events_ticket_quantity', true );
        $google_seo_events_tickets_price_valid = get_post_meta( $post->ID, $prefix.'events_tickets_price_valid', true );
        $google_seo_events_tickets_currency = get_post_meta( $post->ID, $prefix.'events_tickets_currency', true );

        $smack_google_seo_schema_events = '';

        $smack_google_seo_schema_events .= '<div style="display: none;" itemscope itemtype="http://schema.org/Event">
                                        <a itemprop="url" href="'.esc_url($google_seo_events_url).'">
                                        <span itemprop="name"> '.$google_seo_events_summary.' </span>
                                        </a>
                                        <meta itemprop="startDate" content="'.$google_seo_events_startdate.'">'.$google_seo_events_startdate.'
                                        <meta itemprop="endDate" content="'.$google_seo_events_enddate.'">'.$google_seo_events_enddate.'
                                        <meta itemprop="description" content="'.$google_seo_events_description.'">'.$google_seo_events_description.'
                                        <img itemprop="image" src="'. $google_seo_events_photo .'" />'.$google_seo_events_photo.'
                                        <meta itemprop="performer" content="'.$google_seo_events_performer.'">'.$google_seo_events_performer.'
                                        <div itemprop="location" itemscope itemtype="http://schema.org/Place">

                                        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span itemprop="streetAddress">'.$google_seo_events_street_address.'</span>
                                        <span itemprop="addressLocality">'.$google_seo_events_locality.'</span>
                                        <span itemprop="addressRegion">'.$google_seo_events_region.'</span>
                                        <div itemprop="addressCountry" itemscope itemtype="http://schema.org/Country">
                                        <span itemprop="name"> '.$google_seo_events_country.' </span>
                                        </div>
                                        </div>
                                        <span itemprop="name"> '.$google_seo_events_summary.' </span>
                                        </div>
                                        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <span itemprop="name">'.$google_seo_offer_name.'</span>
                                        <span itemprop="category">'.$google_seo_offer_category.'</span>
                                        <span itemprop="price">'.$google_seo_events_ticket_price.'</span>
                                        <span itemprop="priceCurrency">'.$google_seo_events_tickets_currency.'</span>
                                        <a itemprop="url" href="'.esc_url($google_seo_offer_url).'">
                                        '.$google_seo_events_summary.'
                                        </a>
                                        </div>
                                      </div>';
        return $text.$smack_google_seo_schema_events;
}
function smack_google_seo_schema_add_events() {
        global $post;
        $prefix = 'google_snippets';
        $google_seo_event_name = get_post_meta( $post->ID, $prefix.'events_summary', true );
        if( $google_seo_event_name != '' && !is_home() ) {
                add_filter( "the_content", "smack_google_seo_schema_events" );
        }
}
add_action('wp', 'smack_google_seo_schema_add_events');
